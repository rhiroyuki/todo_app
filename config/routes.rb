Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'tasks#index'
  resources :tasks, only: [:new, :create, :show, :index, :edit, :update, :destroy] do
    member do
      put 'mark'
    end
  end
end
