require 'rails_helper'

RSpec.describe Task, type: :model do
  context 'associations' do
    it { should belong_to :user }
  end

  context 'validations' do
    it { should validate_presence_of(:title) }
  end

  it 'creates a valid task' do
    task = create(:task)
    expect(task).to be_valid
  end

end
