require 'rails_helper'
RSpec.describe TasksController, type: :controller do

  let!(:user) { create(:user) }
  before { sign_in user }

  context 'GET #index' do
    let(:tasks) { create_list(:task, 5, user: user) }
    before { get :index }

    it 'return all user tasks' do
      expect(assigns(:tasks)).to eq tasks
    end

    it 'return code status ok' do
      expect(response).to have_http_status(:ok)
    end

    it 'renders index template' do
      expect(response).to render_template(:index)
    end
  end

  context 'GET #new' do
    before { get :new }
    it 'receives code status ok' do
      expect(response).to have_http_status(:ok)
    end

    it 'assigns a new task to the view' do
      expect(assigns(:task)).to be_a_new(Task)
    end

    it 'renders new template' do
      expect(response).to render_template('new')
    end
  end

  context 'POST #create' do
    context 'valid task' do
      before {
        post :create, params: { task: { title: 'title' } }
      }
      it 'redirects to task page' do
        expect(response).to redirect_to(task_path(assigns(:task)))
      end

      it 'code 302(found) when redirected' do
        expect(response).to have_http_status(:found)
      end
    end

    context 'invalid task params' do
      it 'renders new' do
        post :create, params: { task: { title: '' } }
        expect(response).to render_template(:new)
      end
    end
  end

  context 'GET #show' do
    let! :task { create(:task, user: user) }
    before { get :show, params: { id: task.id } }

    it { expect(assigns(:task)).to eq task }

    it { expect(response).to render_template(:show) }
  end

  context 'GET #edit' do
    let :task { create(:task, user: user) }
    before { get :edit, params: { id: task.id } }

    it { expect(assigns(:task)).to eq task }

    it { expect(response).to render_template(:edit) }
  end

  context 'PUT #update' do
    let! :task { create(:task, user: user) }
    before {
      patch :update, params: { id: task.id, task: { title: 'new' } }
      task.reload
    }

    it { expect(assigns(:task).title).to eq task.title }
    it { expect(response).to redirect_to(root_url) }

  end

  context 'DELETE #destroy' do
    let! :task { create(:task, user: user) }

    it 'redirects to root page' do
      delete :destroy, params: { id: task.id }
      expect(response).to redirect_to root_url
    end

    it 'destroys an element' do
      expect{
        delete :destroy, params: { id: task.id }
      }.to change(Task, :count).by(-1)
    end
  end

  context 'PUT #mark' do
    let! :task { create(:task, user: user) }
    before {
      put :mark, params: { id: task.id }
      task.reload
    }

    it { expect(assigns(:task)).to be_done }
    it { expect(response).to redirect_to root_url }
  end
end
