require 'rails_helper'

feature 'User signs up' do
  scenario 'successfully' do
    user = build(:user)

    visit root_path
    click_on 'Sign up'
    within('form') do
      fill_in 'Email', with: user.email
      fill_in 'Password', with: user.password, id: 'user_password'
      fill_in 'Password confirmation', with: user.password_confirmation
    end
    click_on 'Sign up'

    expect(page).to have_content 'You have signed up successfully'
  end

end
