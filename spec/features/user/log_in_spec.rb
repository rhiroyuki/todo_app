require 'rails_helper'

feature 'User logs in' do
  scenario 'successfully' do
    user = create(:user)

    visit root_path
    within('form') do
      fill_in 'Email', with: user.email
      fill_in 'Password', with: user.password, id: 'user_password'
      click_on 'Log in'
    end

    expect(page).to have_content 'Signed in successfully'
  end
end
