require 'rails_helper'

feature 'user logs out' do
  before do
    user = create(:user)
    login_as user
  end

  scenario 'successfully' do
    visit root_path
    click_on 'Log out'
    expect(page).to have_content 'You need to sign in'
  end
end
