require 'rails_helper'

feature 'User deletes task' do

  before do
    @user = create(:user)
    login_as @user
  end

  scenario 'successfully' do
    task = create(:task, user: @user)
    visit root_path

    click_on 'Delete'
    expect(page).to have_no_content task.title
  end
end
