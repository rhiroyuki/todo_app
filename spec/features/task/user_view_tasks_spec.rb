require 'rails_helper'

feature 'User view tasks at root_path' do

  before do
    @user = create(:user)
    login_as @user
  end

  scenario 'successfully' do
    create_list(:task, 4, user: @user)
    visit root_path
    expect(find('ul.tasks')).to have_selector('li', count: 4)
  end

  scenario 'only see not done tasks' do
    create(:task, user: @user, done: true)
    visit root_path
    within('ul.tasks') do
      expect(page).to have_no_selector('li')
    end
  end

  scenario 'only see his own tasks at root_path' do
    another_user = create :user, email: 'another@mail.com'
    task_from_another_user = create(
      :task,
      title: 'another task',
      user: another_user
    )

    visit root_path
    expect(page).to have_no_content(task_from_another_user.title)
  end
end
