require 'rails_helper'

feature 'User creates task' do
  before do
    user = create(:user)
    login_as user
  end

  scenario 'successfully' do
    task = Task.new(
      title: 'Title',
      description: 'Description',
      due_date: '10/10/2016'
    )

    visit root_path
    click_link 'New task', match: :first

    fill_in 'Title', with: task.title
    fill_in 'Description', with: task.description
    fill_in 'Due date', with: task.due_date
    click_on 'Create'

    expect(page).to have_content('Task created')
  end

  scenario 'unsuccessfully' do
    visit root_path
    click_link 'New task', match: :first

    click_on 'Create'
    expect(page).to have_no_content('Task created')
  end
end
