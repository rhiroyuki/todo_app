require 'rails_helper'

feature 'User mark task as done' do

  before do
    user = create(:user)
    login_as user

    create(:task, user: user)
  end
  scenario 'successfully' do
    visit tasks_path
    click_on 'Mark as done', match: :first

    expect(page).to have_content 'Marked as done'
  end
end
