require 'rails_helper'

feature 'User edits task' do

  before do
    user = create(:user)
    @task = create(:task, user: user)
    login_as user
  end

  scenario 'successfully' do
    visit root_path
    click_link 'Edit task', match: :first

    fill_in 'Title', with: @task.title
    click_on 'Update Task'

    expect(page).to have_content @task.title
  end
end
