class TasksController < ApplicationController
  before_action :authenticate_user!

  def index
    @tasks = current_user.tasks.where(done: false)
  end

  def destroy
    @tasks = Task.find_by(id: params[:id])
    @tasks.destroy
    redirect_to root_url
  end

  def edit
    @task = Task.find_by(id: params[:id])
  end

  def update
    @task = Task.update(params[:id], task_params)
    redirect_to root_url
  end

  def new
    @task = Task.new
  end

  def create
    @task = Task.new(task_params)
    @task.user = current_user
    if @task.save
      redirect_to @task, notice: 'Task created'
    else
      render :new
    end
  end

  def show
    @task = Task.find_by(id: params[:id])
  end

  def mark
    @task = Task.find_by(id: params[:id])
    @task.update(done: true)
    redirect_to root_url, notice: 'Marked as done'
  end

  private

  def task_params
    params.require(:task).permit(
      :title,
      :description,
      :due_date
    )
  end
end
